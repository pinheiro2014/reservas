/*
SQLyog Community v13.0.0 (64 bit)
MySQL - 10.1.31-MariaDB : Database - reservas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`reservas` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `reservas`;

/*Table structure for table `res_labortorios` */

DROP TABLE IF EXISTS `res_labortorios`;

CREATE TABLE `res_labortorios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `res_labortorios` */

/*Table structure for table `res_reserva` */

DROP TABLE IF EXISTS `res_reserva`;

CREATE TABLE `res_reserva` (
  `id` int(11) NOT NULL,
  `id_laboratorio` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `tiutlo` varchar(20) NOT NULL,
  PRIMARY KEY (`id`,`id_laboratorio`,`id_usuario`,`start`,`end`,`tiutlo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `res_reserva` */

insert  into `res_reserva`(`id`,`id_laboratorio`,`id_usuario`,`start`,`end`,`tiutlo`) values 
(1,2,3,'2018-06-05 08:00:00','2018-06-05 11:00:00','aula');

/*Table structure for table `res_usuarios` */

DROP TABLE IF EXISTS `res_usuarios`;

CREATE TABLE `res_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios` varchar(20) DEFAULT NULL,
  `senha` varchar(11) DEFAULT NULL,
  `perfil` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `res_usuarios` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
